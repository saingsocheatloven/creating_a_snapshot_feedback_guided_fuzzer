use std::os::unix::process::CommandExt;
use std::process::Command;

use nix::errno;
use nix::sys::ptrace;
use nix::sys::signal::Signal;
use nix::sys::wait::{waitpid, WaitStatus};
use nix::unistd::Pid;

pub fn spawn(cmd: &String, args: &[String]) -> Pid {
    let child = unsafe {
        println!("Spawning {}", cmd);
        Command::new(cmd)
            .args(args)
            .pre_exec(|| {
                ptrace::traceme().expect("Could not trace process");
                Ok(())
            })
            .spawn()
            .expect(&format!("Could not spawn {:?} with args {:?}", cmd, args))
    };

    let res = Pid::from_raw(child.id() as i32);

    match waitpid(res, None) {
        Ok(WaitStatus::Stopped(_, Signal::SIGTRAP)) => {
            ptrace::cont(res, None).expect("Should have continued");
        }
        _ => println!("COULD NOT START"),
    }

    res
}

#[allow(dead_code)]
#[repr(u8)]
pub enum WatchpointBreakType {
    OnExec = 0,
    OnWrite = 1,
    OnRw = 3,
}

#[allow(dead_code)]
#[repr(u8)]
pub enum WatchpointSize {
    One = 0,
    Two = 1,
    Four = 3,
    Eight = 2,
}

pub fn set_watchpoint(child_pid: Pid, mut addr: u64, size: WatchpointSize, reset: bool) {
    unsafe {
        let debug_reg_off = offset_of!(libc::user, u_debugreg);
        let mut debug_ctl_val: u32 = libc::ptrace(
            ptrace::Request::PTRACE_PEEKUSER as ptrace::RequestType,
            child_pid,
            debug_reg_off + (8 * 7),
            0,
        ) as u32;

        let l0: u32 = 0;
        let _g0: u32 = 1;
        let le: u32 = 8;
        let ge: u32 = 9;
        let reserved_10: u32 = 10;
        let gd: u32 = 13;
        let dr0_break: u32 = 16; // 16-18
        let dr0_len: u32 = 18; // 18-20

        if !reset {
            // enable for local tasks only
            debug_ctl_val |= 1 << l0; // l0
            debug_ctl_val |= (u32::from(WatchpointBreakType::OnRw as u8) << dr0_break) as u32;
            debug_ctl_val |= u32::from(size as u8) << dr0_len;

            // https://www.intel.com/content/dam/support/us/en/documents/processors/pentium4/sb/253669.pdf
            //
            // LE and GE (local and global exact breakpoint enable) flags (bits 8, 9) —
            //
            //     This feature is not supported in the P6 family processors, later IA-32 processors,
            //     and Intel 64 processors. When set, these flags cause the processor to detect the
            //     exact instruction that caused a data breakpoint condition. For backward and
            //     forward compatibility with other Intel processors, we recommend that the LE and
            //     GE flags be set to 1 if exact breakpoints are required.
            //
            // GD (general detect enable) flag (bit 13) —
            //
            //     Enables (when set) debugregister protection, which causes a debug exception to be generated prior to any
            //     MOV instruction that accesses a debug register. When such a condition is
            //     detected, the BD flag in debug status register DR6 is set prior to generating the
            //     exception. This condition is provided to support in-circuit emulators.
            //     When the emulator needs to access the debug registers, emulator software can
            //     set the GD flag to prevent interference from the program currently executing on
            //     the processor.
            //     The processor clears the GD flag upon entering to the debug exception handler,
            //     to allow the handler access to the debug registers.
            debug_ctl_val |= 1 << le;
            debug_ctl_val |= 1 << ge;
            debug_ctl_val |= 1 << gd;
            debug_ctl_val |= 1 << reserved_10;
        } else {
            debug_ctl_val &= !(1 << l0);
            debug_ctl_val &= !(1 << le);
            debug_ctl_val &= !(1 << ge);
            debug_ctl_val &= !(1 << gd);
            addr = 0;
        }

        // https://docs.rs/libc/0.2.62/libc/struct.user.html#structfield.u_debugreg
        // https://docs.rs/memoffset/0.2.1/memoffset/macro.offset_of.html
        // https://stackoverflow.com/questions/8941711/is-it-possible-to-set-a-gdb-watchpoint-programmatically
        let res = libc::ptrace(
            ptrace::Request::PTRACE_POKEUSER as ptrace::RequestType,
            child_pid,
            debug_reg_off,
            addr,
        );
        if res != 0 {
            let error = errno::from_i32(errno::errno());
            panic!("Error: {}", error);
        }

        let res = libc::ptrace(
            ptrace::Request::PTRACE_POKEUSER as ptrace::RequestType,
            child_pid,
            debug_reg_off + (8 * 7),
            debug_ctl_val,
        );
        if res != 0 {
            let error = errno::from_i32(errno::errno());
            panic!("Error: {}", error);
        }
    };
}
