extern crate nix;
extern crate rand;
#[macro_use]
extern crate memoffset;

use std::env::args;
use std::process::exit;
use std::time;

use nix::sys::ptrace;
use nix::sys::signal::Signal;
use nix::sys::wait::{waitpid, WaitStatus};
use nix::unistd::Pid;

mod debug;
mod mutate;
mod snapshot;
mod utils;

use debug::{set_watchpoint, spawn, WatchpointSize};
use mutate::mutate;
use snapshot::Snapshot;
use utils::ptrace_get_fpregs;

fn run_once(
    child_pid: Pid,
    snapshot: &Snapshot,
    overwrite_data_addr: u64,
    fuzz_data: &[u8],
) -> bool {
    snapshot.restore(vec![(overwrite_data_addr as usize, fuzz_data)]);
    ptrace::cont(child_pid, None).expect("Could not continue process");

    let res = match waitpid(child_pid, None) {
        Ok(WaitStatus::Stopped(_, Signal::SIGTRAP)) => {
            // hit final breakpoint, simply fall through
            false
        }
        Ok(WaitStatus::Exited(_, status)) => {
            println!("Process exited with status {}", status);
            false
        }
        Ok(WaitStatus::Stopped(_, signal)) => {
            println!("CRASH! signal: {}", signal);
            true
        }
        Ok(WaitStatus::Signaled(_, signal, val)) => {
            println!("Signaled: {} val: {}", signal, val);
            false
        }
        Err(_) => {
            println!("Could not waitpid");
            false
        }
        _ => {
            println!("Other event");
            false
        }
    };

    res
}

fn main() {
    let prog_args: Vec<String> = args().collect();
    if prog_args.len() != 3 {
        println!("USAGE: {} CMD CMD_INPUT", prog_args[0]);
        exit(1);
    }

    let mut _overwrite_data_addr: Option<u64> = None;
    let mut _overwrite_data_max_len: Option<usize> = None;
    let mut snapshot: Option<Snapshot> = None;

    let child_pid = spawn(&prog_args[1], &prog_args[2..]);
    println!("Spawned child process {}", child_pid);

    loop {
        match waitpid(child_pid, None) {
            Ok(WaitStatus::Exited(_, status)) => {
                println!("Process exited with status {}", status);
                break;
            }
            Ok(WaitStatus::Stopped(_, Signal::SIGTRAP)) => {
                println!("Debugee stopped");
                let regs = ptrace::getregs(child_pid).unwrap();

                // First breakpoint, rax should be a pointer to memory that we
                // want to set a breakpoint on
                if regs.rcx == 0xf00dfeed {
                    println!("Received tagged memory address");
                    println!("| bp addr: {:x}", regs.rax);
                    println!("| max_mem_len: {}", regs.rbx);
                    println!("| rip: {:x}", regs.rip);
                    _overwrite_data_addr = Some(regs.rax);
                    _overwrite_data_max_len = Some(regs.rbx as usize);
                    // has to be on a word boundary (can't be an odd number)
                    set_watchpoint(child_pid, regs.rax & !1, WatchpointSize::Two, false);

                // Final breakpoint - should not happen yet! this means our
                // data was never accessed
                } else if regs.rcx == 0xfeedf00d {
                    panic!("Should have hit our watchpoint first");

                // Hit the read breakpoint!
                } else {
                    println!("Watchpoint hit");
                    println!("| rip: {:x}", regs.rip);
                    println!("| rax: {:x}", regs.rax);
                    println!("| rbx: {:x}", regs.rbx);
                    println!("| rcx: {:x}", regs.rcx);
                    println!("| rdx: {:x}", regs.rdx);
                    println!("| rdi: {:x}", regs.rdi);
                    println!("| rsi: {:x}", regs.rsi);

                    // clear the watchpoint before saving the register state
                    set_watchpoint(child_pid, 0, WatchpointSize::Two, true);

                    let fpregs = ptrace_get_fpregs(child_pid).unwrap();
                    let mut tmp = Snapshot::new(child_pid, regs, fpregs);
                    tmp.take();
                    snapshot = Some(tmp);

                    break;
                }
                ptrace::cont(child_pid, None).expect("Should have continued");
            }
            Ok(WaitStatus::Continued(_)) => println!("Continuing"),
            Ok(WaitStatus::PtraceEvent(pid, signal, v)) => {
                println!(
                    "ptrace event: pid: {:?}, signal: {:?}, v: {:?}",
                    pid, signal, v
                );
            }
            Ok(WaitStatus::Signaled(pid, signal, val)) => {
                println!(
                    "ptrace signaled: pid: {:?}, signal: {:?}, val: {:?}",
                    pid, signal, val
                );
            }
            Ok(WaitStatus::StillAlive) => println!("Still alive"),
            Ok(WaitStatus::Stopped(pid, signal)) => {
                println!("Stopped: pid: {:?}, signal: {:?}", pid, signal);
                break;
            }
            Ok(WaitStatus::PtraceSyscall(pid)) => println!("Syscall: {:?}", pid),
            Err(v) => println!("Error!: {:?}", v),
        }
    }

    let snapshot = snapshot.expect("Snapshot was never taken!");

    println!("Starting main snapshot loop now");

    let start = time::Instant::now();
    let mut iters = 0;
    let overwrite_data_addr = _overwrite_data_addr.unwrap();
    let overwrite_data_max_len = _overwrite_data_max_len.unwrap();
    let mut rand = rand::thread_rng();
    let mut input_data: Vec<u8> = vec![0; overwrite_data_max_len];
    input_data.reserve(overwrite_data_max_len);

    let print_status = |x| {
        let end = time::Instant::now();
        println!(
            "{} iters {:.2} iters/s",
            x,
            (x as f64 / (end - start).as_secs_f64()),
        );
    };

    loop {
        iters += 1;
        if iters % 0x7fff == 0 {
            print_status(iters);
        }

        mutate(&mut rand, &mut input_data);

        let crashed = run_once(child_pid, &snapshot, overwrite_data_addr, &input_data);

        if crashed {
            break;
        }
    }

    let _ = ptrace::kill(child_pid);

    println!("Final stats:");
    print_status(iters);
    println!("{} total iterations", iters);
}
